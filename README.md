# Drawer navigation coding challenge

I wasn't sure if I was supposed to add interactivity such as link hover effects, etc. so I just kept it simple. I added a few basic
UI unit tests for each component--nothing complex, just enough to make sure everything is functioning as it should without needing
to manually fire up the app.

I didn't add any fonts or variables for easier styling, and just kept it very basic with CSS. If you want I could refactor the
styling to CSS modules, styled components, sass, or any other library/methodology, just let me know.

You can find screenshots of the app [here.](https://imgur.com/a/XlozrHq)

It's built using CRA (create react app) so it's super simple to fire up if you want to take a look at the transitions, etc.

If you have any questions feel free to email me :)
