import React, { Component } from "react";
import "./App.css";

import Nav from "./components/Nav";
import Menu from "./components/Menu";
import Blurred from "./components/Blurred";

class App extends Component {
  state = {
    showMenu: false
  };

  toggleMenu = () => {
    this.setState((prevState) => ({ showMenu: !prevState.showMenu }));
  };

  handleBlur = () => {
    this.setState(() => ({ showMenu: false }));
  };

  render() {
    const { showMenu } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <Nav toggleMenu={this.toggleMenu} />
          <Menu showMenu={showMenu} />
          {showMenu && <Blurred handleBlur={this.handleBlur} />}
          <h1>Ethereum Balance</h1>
          <p className="App-currency">
            4.099 <small>ETH</small>
          </p>
        </header>
      </div>
    );
  }
}

export default App;
