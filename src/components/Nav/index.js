import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLongArrowAltLeft, faBars } from "@fortawesome/free-solid-svg-icons";
import "./Nav.css";

const Nav = ({ toggleMenu }) => (
  <nav className="Nav">
    <button data-testid="back-button">
      <FontAwesomeIcon icon={faLongArrowAltLeft} className="Nav-icon" />
    </button>
    <div className="Nav-switch">switch</div>
    <button onClick={toggleMenu} data-testid="menu-button">
      <FontAwesomeIcon icon={faBars} className="Nav-icon" />
    </button>
  </nav>
);

export default Nav;
