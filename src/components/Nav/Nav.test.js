import React from "react";
import { render } from "react-testing-library";

import Nav from "./index";

describe("<Nav />", () => {
  it("should render back button", () => {
    const { getByTestId } = render(<Nav />);
    expect(getByTestId("back-button")).toBeTruthy();
  });

  it("should render menu button", () => {
    const { getByTestId } = render(<Nav />);
    expect(getByTestId("menu-button")).toBeTruthy();
  });

  it("should call toggleMenu on menu button click", () => {
    const toggleMenu = jest.fn();
    const { getByTestId } = render(<Nav toggleMenu={toggleMenu} />);
    getByTestId("menu-button").click();
    expect(toggleMenu).toHaveBeenCalledTimes(1);
  });
});
