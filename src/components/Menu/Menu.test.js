import React from "react";
import { render } from "react-testing-library";

import Menu from "./index";

describe("<Menu />", () => {
  it("should add open class to menu div if showMenu true", () => {
    const { getByTestId } = render(<Menu showMenu={true} />);
    expect(getByTestId("menu").classList.contains("open")).toBeTruthy();
  });

  it("should not render open class if showMenu false", () => {
    const { getByTestId } = render(<Menu showMenu={false} />);
    expect(getByTestId("menu").classList.contains("open")).not.toBeTruthy();
  });

  it("should render sign out button if user is logged in", () => {
    const { getByText } = render(<Menu showMenu={true} />);
    expect(getByText("Sign Out")).toBeTruthy();
  });
});
