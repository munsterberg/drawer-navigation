import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWallet, faDollarSign, faTag } from "@fortawesome/free-solid-svg-icons";
import "./Menu.css";

class Menu extends Component {
  render() {
    return (
      <div className={`Menu ${this.props.showMenu ? "open" : ""}`} data-testid="menu">
        <div>
          <FontAwesomeIcon icon={faWallet} className="Menu-icon" />
          <h2>Wallet</h2>
          <ul>
            <li>
              <a href="/">All Coins</a>
            </li>
            <li>
              <a href="/">Borrow</a>
            </li>
            <li>
              <a href="/">Contacts</a>
            </li>
            <li>
              <a href="/">Settings</a>
            </li>
          </ul>
        </div>
        <div>
          <FontAwesomeIcon icon={faDollarSign} className="Menu-icon" />
          <h2>Crowdsale</h2>
        </div>
        <div>
          <FontAwesomeIcon icon={faTag} className="Menu-icon" style={{ paddingRight: "15px" }} />
          <h2>Special Offers</h2>
        </div>
        <div className="Menu-divider" style={{ marginTop: "30px" }} />
        <div className="Menu-footer">
          <button className="Menu-button">Sign Out</button>
          <div className="Menu-logo">Logo</div>
        </div>
      </div>
    );
  }
}

export default Menu;
