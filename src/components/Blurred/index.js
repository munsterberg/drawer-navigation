import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import "./Blurred.css";

const Blurred = ({ handleBlur }) => (
  <div className="Blurred" onClick={handleBlur} data-testid="blurred">
    <FontAwesomeIcon icon={faTimes} className="Blurred-exit" onClick={handleBlur} data-testid="exit-icon" />
  </div>
);

export default Blurred;
