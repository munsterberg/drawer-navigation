import React from "react";
import { render } from "react-testing-library";

import Blurred from "./index";

describe("<Blurred />", () => {
  it("should render fontawesome icon for exit", () => {
    const { getByTestId } = render(<Blurred />);
    expect(getByTestId("exit-icon")).toBeTruthy();
  });

  it("should call handleBlur on exit button click", () => {
    const handleBlur = jest.fn();
    const { getByTestId } = render(<Blurred handleBlur={handleBlur} />);
    getByTestId("exit-icon").click();
    expect(handleBlur).toHaveBeenCalled();
  });

  it("shoudl call handleBlur on div click", () => {
    const handleBlur = jest.fn();
    const { getByTestId } = render(<Blurred handleBlur={handleBlur} />);
    getByTestId("blurred").click();
    expect(handleBlur).toHaveBeenCalled();
  });
});
